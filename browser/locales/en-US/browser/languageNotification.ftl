# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# $language is the language Tor Browser is displayed in (already translated)
language-notification-label-system = { -brand-short-name } has set your display language to { $language } based on your system’s language.
# This is shown when the system language is not supported, so we fall back to another language instead.
# $language is the language Tor Browser is displayed in (already translated).
language-notification-label = { -brand-short-name } has set your display language to { $language }.
language-notification-button = Change Language…
